﻿using System.Diagnostics;
using System.Numerics;
using System.Text;
using OVAPI.Code;

namespace WanderOV
{
    internal class Vehicle
    {
        internal string owner;
        internal string linePrivateNumber;
        internal string journeyNumber;
        internal string vehicleNumber;
        internal string punctuality;
        internal float rdx;
        internal float rdy;
        internal long lastUpdate;
        internal long lastAdvanceTime;
        internal bool reachedLastStop;
        internal bool mustTeleport;

        internal IJourney journey;

        internal float speedMtrPerSec;
        internal float interpolatedX;
        internal float interpolatedY;
        internal float catmullX;
        internal float catmullY;
        internal float angle;

        internal int prevPointIdxInJourney=-1;
        internal int prevInterpolatedPointIdxInJourney=-1;

        internal bool Valid => (rdx!=0 && rdy!=0 && journey != null /*&& angle != 0*/ && interpolatedX != 0 && interpolatedY != 0);
    }


    public class ActiveOV
    {
        Dictionary<string, Vehicle> vehicles;
        Stopwatch stopwatch;
        INetwork network;

        public ActiveOV( INetwork _network )
        {
            network   = _network;
            vehicles  = new Dictionary<string, Vehicle>();
            stopwatch = new Stopwatch();
            stopwatch.Start();
        }

        public string GetStateAsCSV()
        {
            DeleteInactiveVehicles();
            return VehiclesToCSV();
        }

        public void AdvanceSimulationToNow()
        {
            var timeNow = stopwatch.ElapsedMilliseconds;

            foreach (var vkp in vehicles)
            {
                var vehicle = vkp.Value;
                if (vehicle.journey == null) // Journey is immediately assigned when vehicle is updated. If journey is null the journey is not available in the database.
                    continue;

                if (vehicle.reachedLastStop)
                    continue;

                float elapsedSeconds     = (timeNow - vehicle.lastAdvanceTime)/1000.0f;
                vehicle.lastAdvanceTime  = timeNow;
                float advanceDistanceMtr = elapsedSeconds*vehicle.speedMtrPerSec;

                if (NetworkUtils.FindPositionInJourney(
                     vehicle.journey,
                     ref vehicle.prevInterpolatedPointIdxInJourney,
                     new Vector2( vehicle.interpolatedX, vehicle.interpolatedY ),
                     advanceDistanceMtr,
                     out (Vector2 point, float dist, Vector2 dir) interpolant,
                     out Vector2 catmullPnt
                    ))
                {
                    vehicle.interpolatedX = interpolant.point.X;
                    vehicle.interpolatedY = interpolant.point.Y;
                    vehicle.catmullX = catmullPnt.X;
                    vehicle.catmullY = catmullPnt.Y;
                    Debug.Assert( MathF.Abs( interpolant.dir.Length()-1 ) < 0.001f );
                    vehicle.angle = MathF.Atan2( interpolant.dir.X, interpolant.dir.Y ) * 57.29578f; // To Degrees.
                    if (float.Abs( vehicle.catmullX ) < 0.01f)
                    {
                        int kt2 = 0;
                    }
                }
                else
                {
                    vehicle.reachedLastStop = true;
                }

                if ( float.Abs( vehicle.catmullX ) < 0.01f )
                {
                    int kt = 0;
                }
            }
        }

        public void UpdateVehicles( Dictionary<string, VehicleInfo> updatedVehicles )
        {
            if (updatedVehicles == null)
                return;

            long lastTime = stopwatch.ElapsedMilliseconds;

            foreach (var kvp in updatedVehicles)
            {
                var updatedVehicle = kvp.Value;

                if (!vehicles.TryGetValue( updatedVehicle.ID, out var vehicle ))
                {
                    vehicle = new()
                    {
                        owner = updatedVehicle.Owner,
                        vehicleNumber = updatedVehicle.Number
                    };
                    vehicles.Add( updatedVehicle.ID, vehicle );
                }

                UpdateVehicleInfo( vehicle, updatedVehicle, lastTime );
                UpdateVehiclePosition( vehicle, updatedVehicle );
                UpdateVehicleJourney( vehicle, updatedVehicle, lastTime );
            }
        }

        private void UpdateVehicleInfo( Vehicle vehicle, VehicleInfo updatedVehicle, long lastTime )
        {
            vehicle.punctuality = updatedVehicle.infos[(int)VehicleInfoType.Punctuality];
            vehicle.linePrivateNumber = updatedVehicle.infos[(int)VehicleInfoType.LinePrivateNumber];
            vehicle.lastUpdate = lastTime;
        }

        private void UpdateVehiclePosition( Vehicle vehicle, VehicleInfo updatedVehicle )
        {
            if (updatedVehicle.infos[(int)VehicleInfoType.Rdx] == null)
            {
                IStop stop;
                if (updatedVehicle.infos[(int)VehicleInfoType.StopCode] != null &&
                    (stop = network.FindStop( vehicle.owner, updatedVehicle.infos[(int)VehicleInfoType.StopCode] )) != null)
                {
                    vehicle.rdx = stop.Point.X;
                    vehicle.rdy = stop.Point.Y;
                }
            }
            else if (float.TryParse( updatedVehicle.infos[(int)VehicleInfoType.Rdx], out var newRdx ) &&
                     float.TryParse( updatedVehicle.infos[(int)VehicleInfoType.Rdy], out var newRdy ))
            {
                vehicle.rdx = newRdx;
                vehicle.rdy = newRdy;
            }
        }

        private void UpdateVehicleJourney( Vehicle vehicle, VehicleInfo updatedVehicle, long lastTime )
        {
            if (vehicle.journeyNumber != updatedVehicle.infos[(int)VehicleInfoType.JourneyNumber])
            {
                vehicle.journeyNumber = updatedVehicle.infos[(int)VehicleInfoType.JourneyNumber];
                ResetVehicleJourney( vehicle, lastTime );
            }

            AdjustVehicleSpeed( vehicle );
        }

        private void ResetVehicleJourney( Vehicle vehicle, long lastTime )
        {
            vehicle.mustTeleport = true;
            vehicle.journey = network.FindJourney( vehicle.owner, vehicle.journeyNumber, vehicle.linePrivateNumber );
            vehicle.prevPointIdxInJourney = -1;
            vehicle.prevInterpolatedPointIdxInJourney = -1;
            vehicle.lastAdvanceTime = lastTime;
            vehicle.speedMtrPerSec = 25 / 3.6f; // initial speed
            vehicle.interpolatedX = vehicle.rdx;
            vehicle.interpolatedY = vehicle.rdy;
            vehicle.reachedLastStop = false;
        }

        private void AdjustVehicleSpeed( Vehicle vehicle )
        {
            if (vehicle.Valid &&
                NetworkUtils.FindPositionInJourney(
                    vehicle.journey,
                    ref vehicle.prevPointIdxInJourney,
                    new Vector2( vehicle.rdx, vehicle.rdy ),
                    0,
                    out _,
                    out _ ))
            {
                if (vehicle.prevPointIdxInJourney != -1 && vehicle.prevInterpolatedPointIdxInJourney != -1)
                {
                    int segmentDelta = vehicle.prevPointIdxInJourney - vehicle.prevInterpolatedPointIdxInJourney;
                    if (segmentDelta > 0)
                    {
                        vehicle.speedMtrPerSec *= (1 + 0.2f * segmentDelta);
                    }
                    else if (segmentDelta < 0)
                    {
                        vehicle.speedMtrPerSec /= (1 + 0.2f * -segmentDelta);
                    }

                    vehicle.speedMtrPerSec = Math.Clamp( vehicle.speedMtrPerSec, 5, 15 );
                }
            }
        }


        void DeleteInactiveVehicles()
        {
            long timeNow = stopwatch.ElapsedMilliseconds;
            var pendingRemoves = new List<string>();
            foreach (var v in vehicles)
            {
                if (timeNow - v.Value.lastUpdate > 1000 * 60*3)
                {
                    pendingRemoves.Add( v.Key );
                }
            }
            pendingRemoves.ForEach( pr => vehicles.Remove( pr ) );
        }

        string VehiclesToCSV()
        {
            if (vehicles==null||vehicles.Count==0)
                return null;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine( $"mustTeleport,owner,displayLineNumb,privLineNumb,journyNum,vehicleNum,punctuality,rdx,rdy,interpolated-rdx,interpolated-rdy,angleDeg,speedMtrps" );
            foreach (var kvp in vehicles)
            {
                var v = kvp.Value;
                if (v.Valid)
                {
                    var pubNumber = v.journey.Line.LineDisplayNumber;
                    if ( float.Abs(v.catmullX) < 0.01f )
                    {
                        int kt = 0;
                    }
                    sb.AppendLine( $"{v.mustTeleport},{v.owner},{pubNumber},{v.linePrivateNumber},{v.journeyNumber},{v.vehicleNumber},{v.punctuality},{v.rdx},{v.rdy},{v.catmullX},{v.catmullY},{v.angle},{v.speedMtrPerSec}" );
                    v.mustTeleport=false;
                }
            }
            return sb.ToString();
        }
    }

}   