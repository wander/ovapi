﻿using System.Diagnostics;
using System.IO.Compression;
using System.Numerics;
using System.Reflection;
using System.Xml;

namespace OVAPI.Code.OVParsers
{
    public static class XmlUtils
    {
        public static XmlElement GetChild( this XmlElement elem, string name )
        {
            for (int i = 0;i <elem.ChildNodes.Count;i++)
            {
                var child = elem.ChildNodes[i];
                if (child.Name==name)
                    return child as XmlElement;
            }
            return null;
        }
    }

    public class Netex : INetwork
    {
        class RoutePoint
        {
            internal float rdx, rdy;

            internal Vector2 AsVector => new Vector2( rdx, rdy );
        }

        class RouteLink
        {
            internal List<(float rdx, float rdy)> points; // Optional
            internal RoutePoint from;
            internal RoutePoint to;
        }

        class Route : IRoute
        {
            internal Line line;
            internal List<RouteLink> links;
            internal List<(Vector2 point, float dst, Vector2 dir)> points;

            public int NumPoints => points.Count;
            public (Vector2 point, float dist, Vector2 dir) Point( int idx ) => points[idx];
        }

        class Line : ILine
        {
            internal string name;
            internal string displayNumber;
            internal string transportType;
            internal string linePlanningNumber;
            internal Route route;

            public string Name => name;
            public string LinePlanningNumber => linePlanningNumber;
            public string LineDisplayNumber => displayNumber;
            public string TransportType => transportType;
        }

        class Destination : IDestination
        {
            internal string frontText;
            internal string sideText;
            internal List<string> vias;

            public string Name => frontText;
            public string Via => throw new NotImplementedException();
        }

        class Stop : IStop
        {
            internal string name;
            internal float rdx, rdy;

            public string Name => name;
            public Vector2 Point => new Vector2( rdx, rdy );
        }

        class JourneyPattern : IJourney
        {
            internal Line line;
            internal Route route;
            internal Destination destination;
            internal List<(Stop, Destination)> stops;

            public bool Valid => line!=null && route !=null /* && destination!= null */;

            public ILine Line => line;

            public IRoute Route => route;

            public int NumLinks => route.links.Count;

            public IDestination Destination => destination;

            public (IStop, IDestination) Stop( int i ) => ((IStop, IDestination))stops[i];

            public int NumStops => stops.Count;
        }

        string activeOwner = string.Empty;
        Dictionary<string, RoutePoint> routePoints = new();
        Dictionary<string, RouteLink> routeLinks = new();
        Dictionary<string, Route> routes = new();
        Dictionary<string, Line> lines = new();
        Dictionary<string, Destination> destinations = new();
        Dictionary<string, Stop> stops = new();
        Dictionary<string, JourneyPattern> journeyPatterns = new();
        Dictionary<string, Dictionary<string, List<JourneyPattern>>> journeyNumbersToPattern = new();

        public void PrintStats()
        {
            Console.WriteLine( $"Num routePoints {routePoints.Count}" );
            Console.WriteLine( $"Num routeLinks {routeLinks.Count}" );
            Console.WriteLine( $"Num routes {routes.Count}" );
            Console.WriteLine( $"Num lines {lines.Count}" );
            Console.WriteLine( $"Num destinations {destinations.Count}" );
            Console.WriteLine( $"Num stops {stops.Count}" );
            Console.WriteLine( $"Num journeys {journeyPatterns.Count}" );
        }

        void ParseElementList( XmlDocument doc, string name, Action<XmlElement> cb )
        {
            var elemList = doc.GetElementsByTagName(name);
            foreach (var elem in elemList)
            {
                cb( elem as XmlElement );
            }
        }

        void LoadNetexFile( string xml )
        {
            var doc = new XmlDocument();
            doc.LoadXml( xml );

            activeOwner = doc.GetElementsByTagName( "ParticipantRef" ).Item( 0 ).InnerText;

            ParseElementList( doc, "RoutePoint", LoadRoutePoint );
            ParseElementList( doc, "RouteLink", LoadRouteLink );
            ParseElementList( doc, "Line", LoadLine );
            ParseElementList( doc, "Route", LoadRoute );
            ParseElementList( doc, "DestinationDisplay", LoadDestination );
            ParseElementList( doc, "ScheduledStopPoint", LoadStop );
            ParseElementList( doc, "ServiceJourneyPattern", LoadJourneyPattern );
            ParseElementList( doc, "ServiceJourney", LoadJourneyCode );
        }

        void LoadRoutePoint( XmlElement elem )
        {
            var id = elem.GetAttribute("id");
            if (id != null && !routePoints.TryGetValue( id, out _ ))
            {
                try // Only add if everything can be filled in.
                {
                    var rp = new RoutePoint();
                    var pos = elem.GetChild("Location").GetChild("gml:pos").InnerText.Split(' ');
                    if (pos.Length >= 2) // Sometimes nothing is encoded..
                    {
                        rp.rdx = float.Parse( pos[0] );
                        rp.rdy = float.Parse( pos[1] );
                    }
                    routePoints.Add( id, rp );
                }
                catch (Exception e) { Console.WriteLine( e.Message ); }
            }
        }

        void LoadRouteLink( XmlElement elem )
        {
            var id = elem.GetAttribute("id");
            if (id != null && !routeLinks.TryGetValue( id, out _ ))
            {
                try // Only add if everything can be filled in.
                {
                    var link = new RouteLink();
                    link.points = new List<(float rdx, float rdy)>();
                    var lineString = elem.GetChild("gml:LineString");
                    if (lineString != null)
                    {
                        var posList = lineString.GetChild("gml:posList");
                        if (posList != null)
                        {
                            var pos = posList.InnerText.Split(' ', StringSplitOptions.RemoveEmptyEntries);
                            if (pos.Length >= 2 && pos.Length % 2 == 0) // Sometimes nothing is encoded..
                            {
                                for (int i = 0;i < pos.Length;i+= 2)
                                {
                                    link.points.Add( (float.Parse( pos[i] ), float.Parse( pos[i+1] )) );
                                }
                            }
                        }
                    }
                    link.from = routePoints[elem.GetChild( "FromPointRef" ).GetAttribute( "ref" )];
                    link.to   = routePoints[elem.GetChild( "ToPointRef" ).GetAttribute( "ref" )];
                    routeLinks.Add( id, link );
                }
                catch (Exception e) { Console.WriteLine( e.Message ); }
            }
        }

        void LoadRoute( XmlElement elem )
        {
            var id = elem.GetAttribute("id");
            if (id != null && !routes.TryGetValue( id, out _ ))
            {
                try // Only add if everything can be filled in.
                {
                    var route = new Route();
                    route.line  = lines[elem.GetChild( "LineRef" ).GetAttribute( "ref" )];
                    route.links = new List<RouteLink>();
                    var rp = elem.GetChild("pointsInSequence").GetChild("PointOnRoute");
                    while (rp != null)
                    {
                        var routePoint = routePoints[rp.GetChild("RoutePointRef").GetAttribute("ref")];
                        var linkToNext = rp.GetChild("OnwardRouteLinkRef");
                        if (linkToNext != null)
                        {
                            var link = routeLinks[linkToNext.GetAttribute( "ref" )];
                            route.links.Add( link );
                        }
                        rp = rp.NextSibling as XmlElement;
                    }
                    routes.Add( id, route );
                }
                catch (Exception e) { Console.WriteLine( e.Message ); }
            }
        }

        void LoadLine( XmlElement elem )
        {
            var id = elem.GetAttribute("id");
            if (!lines.TryGetValue( id, out _ ))
            {
                try // Only add if everything can be filled in.
                {
                    var line = new Line();
                    line.name = elem.GetChild( "Name" ).InnerText;
                    line.displayNumber = elem.GetChild( "PublicCode" ).InnerText;
                    line.transportType = elem.GetChild( "TransportMode" ).InnerText;
                    line.linePlanningNumber = elem.GetChild( "PrivateCode" ).InnerText;
                    lines.Add( id, line );
                }
                catch (Exception e) { Console.WriteLine( e.Message ); }
            }
        }

        void LoadDestination( XmlElement elem )
        {
            var id = elem.GetAttribute("id");
            if (!destinations.TryGetValue( id, out _ ))
            {
                try // Only add if everything can be filled in.
                {
                    var dest = new Destination();
                    var frontTextNode = elem.GetChild( "FrontText" );
                    if (frontTextNode != null) dest.frontText = frontTextNode.InnerText;
                    var sideTextNode = elem.GetChild("SideText");
                    if (sideTextNode != null) dest.sideText  = sideTextNode.InnerText;
                    dest.vias = new List<string>();
                    var vias = elem.GetChild("vias");
                    if (vias != null)
                    {
                        var via = vias.GetChild("Via");
                        while (via != null)
                        {
                            var name = via.GetChild("Name").InnerText;
                            dest.vias.Add( name );
                            via = via.NextSibling as XmlElement;
                        }
                    }
                    destinations.Add( id, dest );
                }
                catch (Exception e) { Console.WriteLine( e.Message ); }
            }
        }

        void LoadStop( XmlElement elem )
        {
            var id = elem.GetAttribute("id");
            if (!stops.TryGetValue( id, out _ ))
            {
                try // Only add if everything can be filled in.
                {
                    var stop  = new Stop();
                    stop.name = elem.GetChild( "Name" ).InnerText;
                    var cnt = elem.GetChild("Location").GetChild("gml:pos").InnerText;
                    var pos = cnt.Split(' ');
                    if (pos.Length >= 2)
                    {
                        stop.rdx = float.Parse( pos[0] );
                        stop.rdy = float.Parse( pos[1] );
                        stops.Add( id, stop );
                    }
                }
                catch (Exception e) { Console.WriteLine( e.Message ); }
            }
        }

        void LoadJourneyPattern( XmlElement elem )
        {
            try // Only add if everything can be filled in.
            {
                var id = elem.GetAttribute("id");
                if (!journeyPatterns.TryGetValue( id, out _ ))
                {

                    var journey = new JourneyPattern();
                    //journey.id = id;
                    journey.route = routes[elem.GetChild( "RouteRef" ).GetAttribute( "ref" )];
                    var destElem = elem.GetChild( "DestinationDisplayRef" );
                    if (destElem != null)
                    {
                        var destRef = destElem.GetAttribute( "ref" );
                        if (destinations.TryGetValue( destRef, out var firstDestination ))
                        {
                            journey.destination = firstDestination;
                        }
                    }
                    var curDestination = journey.destination;
                    journey.line  = journey.route.line;
                    journey.stops = new List<(Stop, Destination)>();
                    var point = elem.GetChild("pointsInSequence").GetChild("StopPointInJourneyPattern");
                    while (point != null)
                    {
                        var scheduledStop = point.GetChild("ScheduledStopPointRef");
                        if (scheduledStop != null)
                        {
                            if (stops.TryGetValue( scheduledStop.GetAttribute( "ref" ), out var stop ))
                            {
                                var destinationDisplay = point.GetChild("DestinationDisplayRef");
                                if (destinationDisplay != null)
                                {
                                    if (destinations.TryGetValue( destinationDisplay.GetAttribute( "ref" ), out var newdest ))
                                    {
                                        curDestination = newdest;
                                    }
                                }
                                journey.stops.Add( (stop, curDestination) );
                            }
                        }
                        point = point.NextSibling as XmlElement;
                    }
                    journeyPatterns.Add( id, journey );
                }
            }
            catch (Exception e)
            {
                Console.WriteLine( e.Message );
            }
        }

        void LoadJourneyCode( XmlElement elem )
        {
            try
            {
                string journeyNumb = elem.GetChild( "PrivateCode" ).InnerText;
                string journeyRef  = elem.GetChild( "ServiceJourneyPatternRef" ).GetAttribute("ref");
                var key = string.Join( ':', activeOwner, journeyNumb );
                if (!journeyNumbersToPattern.TryGetValue( key, out var mapOfLinesToPatterns ))
                {
                    mapOfLinesToPatterns = new Dictionary<string, List<JourneyPattern>>();
                    journeyNumbersToPattern.Add( key, mapOfLinesToPatterns );
                }
                var journeyPattern = journeyPatterns[journeyRef];
                var pviLineNumb = journeyPattern.line.linePlanningNumber;
                if (!mapOfLinesToPatterns.TryGetValue( pviLineNumb, out var listOfPatterns ))
                {
                    listOfPatterns = new List<JourneyPattern>();
                    mapOfLinesToPatterns.Add( pviLineNumb, listOfPatterns );
                }
                listOfPatterns.Add( journeyPattern );
            }
            catch (Exception e)
            {
                Console.WriteLine( e.Message );
            }
        }

        public void LoadAll( string folder )
        {
            string strExeFilePath = Assembly.GetExecutingAssembly().Location;
            string strWorkPath = Path.GetDirectoryName(strExeFilePath);
            string searchPath = Path.Combine(strWorkPath, folder);

            if (!Directory.Exists( searchPath ))
            {
                Console.WriteLine( searchPath + " does not exist, nothing loaded." );
                return;
            }

            var gzFiles = Directory.GetFiles(searchPath, "*.gz", SearchOption.AllDirectories);

            foreach (var gzPath in gzFiles)
            {
                if (string.IsNullOrWhiteSpace( gzPath ))
                    continue;

                if (!gzPath.ToLower().Contains( "netex" ))
                    continue;

                using var gzStream = new GZipStream(File.OpenRead(gzPath), CompressionMode.Decompress);
                using var sr = new StreamReader(gzStream);
                var netexXML = sr.ReadToEnd();

                LoadNetexFile( netexXML );
                Console.WriteLine( "NETEX Loaded " + Path.GetFileName( gzPath ) );
            }

            // Concat link points to routes, so one big array of points.
            List<string> invalidRoutes = new();
            foreach (var kvp in routes)
            {
                var route = kvp.Value;
                route.line.route = route;
                route.points = new List<(Vector2, float, Vector2)>();
                for (int i = 0;i < route.links.Count;i++)
                {
                    var link = route.links[i];

                    // These are the points in the link (if any).
                    if (link.points != null && link.points.Count > 0)
                    {
                        // Sometimes, points are reversed order.
                        var p0   = new Vector2( link.points[0].rdx, link.points[0].rdy );
                        float d1 = (link.to.AsVector-p0).Length();
                        float d2 = (link.from.AsVector-p0).Length();
                        if (d1<d2) // If dist of first point is closer to 'To' than to 'From', reverse the list.
                        {
                            link.points.Reverse();
                        }
                        for (int j = 0;j< link.points.Count;j++)
                        {
                            var pp0 = link.points[j];
                            route.points.Add( (new Vector2( pp0.rdx, pp0.rdy ), 0, Vector2.Zero) );
                        }
                    }
                    else
                    {
                        // There is no navigation info available, only the stops in the route. Remove this route, 
                        // as buses/trams will interpolate through the scenery from stop to stop.
                        invalidRoutes.Add( kvp.Key );
                    }
                }

                // Remove points that are too close to eachother (< 0.5m), that deriving tangent may lead to invalid results.
                for (int i = 0;i < route.points.Count-1;i++)
                {
                    var p0  = route.points[i];
                    for (int j = i+1;j < route.points.Count;j++)
                    {
                        var p1  = route.points[j];
                        var dir = p1.point-p0.point;
                        var len = dir.Length();
                        if (len <= 0.5f)
                        {
                            route.points.RemoveAt( j );
                            j--;
                        }
                        else
                        {
                            Debug.Assert( len > 0.5f );
                            dir /= len;
                            Debug.Assert( MathF.Abs( Vector2.Dot( dir, dir )-1 ) < 0.01f ); // Ensure normalized.
                            route.points[i] = (p0.point, len, dir);
                            break;
                        }
                    }
                }
                if (route.points.Count>=2) // Set last point direction to previous.
                {
                    var lastElem = route.points[route.points.Count-1];
                    lastElem.dir = route.points[route.points.Count-2].dir;
                    route.points[route.points.Count-1] = lastElem;
                }
                else
                {
                    invalidRoutes.Add( kvp.Key );
                }
            }

            // Remove invalid routes.
            invalidRoutes.ForEach( ir => routes.Remove( ir ) );
            Console.WriteLine( $"Found {invalidRoutes.Count} invalid routes." );
            invalidRoutes = null;

            foreach (var kvp in routes)
            {
                var route = kvp.Value;
                route.links = null;
            }
        }

        // Network overrides
        public IJourney FindJourney( string owner, string journeyNumb, string privLineNumber )
        {
            string key = string.Join(':', owner, journeyNumb);
            if (journeyNumbersToPattern.TryGetValue( key, out var lineToJourneys ))
            {
                if (lineToJourneys.TryGetValue( privLineNumber, out var listOfJourneys ))
                {
                    for(int i = 0; i < listOfJourneys.Count; i++)
                    {
                        if (listOfJourneys[i].Valid)
                        {
                            return listOfJourneys[i];
                        }
                    }
                }
            }
            return null;
        }

        public IStop FindStop( string owner, string userStopCode )
        {
            if (!(string.IsNullOrEmpty( owner ) || string.IsNullOrEmpty( userStopCode )))
            {
                string key = string.Join(':', owner, "ScheduledStopPoint", userStopCode);
                if (stops.TryGetValue( key, out var stop ))
                {
                    return stop;
                }
            }
            return null;
        }
    }

}