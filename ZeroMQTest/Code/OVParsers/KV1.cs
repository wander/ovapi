﻿using Sylvan.Data.Csv;
using System.IO.Compression;
using System.Reflection;

namespace OVAPI.Code.OVParsers
{
    public class KV1
    {
        internal enum TransportType
        {
            Bus,
            Tram,
            Ferry,
            Train,
            Unknown
        }

        internal class Line
        {
            internal string Id;
            internal string publicNumber;  // What is shown on display front Bus/Tram. String because might C11.
            internal string fromName;
            internal string toName;
            internal TransportType transportType;
        }

        internal class Destination
        {
            internal string Id;
            internal string mainDest;
            internal string destDetail;
        }

        internal class Stop
        {
            internal string Id; // This is equivalent to UserStopCode (highly confusing..).
            internal float x;
            internal float y;
            internal string name;
            internal string city;
        }

        internal class Link
        {
            internal string fromId;
            internal string toId;
            internal List<(float rdx, float rdy)> points; // list of rdx, rdy points.
        }

        internal class Journey
        {
            internal string Id;
            internal Line line;
            internal Destination dest;
            internal List<Stop> stops;
        }

        internal Dictionary<string, Stop> stops = new Dictionary<string, Stop>();
        internal Dictionary<string, Line> lines = new Dictionary<string, Line>();
        internal Dictionary<string, Destination> destinations = new Dictionary<string, Destination>();
        internal Dictionary<string, Link> links = new Dictionary<string, Link>();
        internal Dictionary<string, Journey> journeys = new Dictionary<string, Journey>();

        string curZipPath;
        string curEntry;

        static string MakeKey( CsvDataReader csv, out string owner, out string code )
        {
            owner = csv.GetString( 3 );
            code = csv.GetString( 4 );
            return string.Join( ':', owner, code );
        }

        void LoadLine( CsvDataReader csv )
        {
            var key = MakeKey(csv, out var owner, out var code);
            if (!lines.ContainsKey( key ))
            {
                var line = new Line();
                line.Id = code;
                line.publicNumber = csv.GetString( 5 );
                string lineName = csv.GetString(6);
                string[] fromTo = lineName.Split('-');
                if (fromTo!=null)
                {
                    if (fromTo.Length > 0)
                        line.fromName = fromTo[0];
                    if (fromTo.Length > 1)
                        line.toName = fromTo[1];
                }
                line.transportType = csv.GetString( 9 ) switch
                {
                    "BUS" => TransportType.Bus,
                    "TRAM" => TransportType.Tram,
                    "TRAIN" => TransportType.Train,
                    "FERRY" => TransportType.Ferry,
                    _ => TransportType.Unknown
                };

                lines.Add( key, line );
            }
            else
            {
                Console.WriteLine( $"Found duplicate line {code} in {curZipPath} and file {curEntry}" );
            }
        }

        void LoadDest( CsvDataReader csv )
        {
            var key = MakeKey(csv, out var owner, out var code);
            if (!destinations.ContainsKey( key ))
            {
                var dest = new Destination();
                dest.Id = code;
                dest.mainDest = csv.GetString( 6 );
                dest.destDetail = csv.GetString( 7 );
                destinations.Add( key, dest );
            }
            else
            {
                // It appears that multiple files have the same destination encoded, this happes because of region traffic. Ignore it.
                // Console.WriteLine( $"Found duplicate destination {code} in {curZipPath} and file {curEntry}" );
            }
        }

        void LoadStop( CsvDataReader csv )
        {
            var key = MakeKey(csv, out var owner, out var code);
            if (!stops.ContainsKey( key ))
            {
                var stop = new Stop();
                stop.Id = code;
                stop.x = csv.GetInt32( 8 );
                stop.y = csv.GetInt32( 9 );
                var desc = csv.GetString(11);
                string[] cityStop = desc.Split(',');
                if (cityStop.Length > 1)
                {
                    stop.name = cityStop[1];
                    stop.city = cityStop[0];
                }
                else if (cityStop.Length > 0)
                {
                    stop.name = cityStop[0];
                }
                stops.Add( key, stop );
            }
            else
            {
                //   Console.WriteLine( $"Found duplicate stop {pointCode} in {curZipPath} and file {curEntry}" );
            }
        }

        void LoadLink( CsvDataReader csv )
        {
            var owner = csv.GetString(3);
            var fromPoint = csv.GetString(4);
            var toPoint = csv.GetString(5);
            var key = string.Join(':', fromPoint, toPoint);
            if (!links.ContainsKey( key ))
            {
                var link = new Link();
                links.Add( key, link );
            }
        }

        void LoadJourney( CsvDataReader csv )
        {
            var key = MakeKey(csv, out var owner, out var code);
            if (!journeys.TryGetValue( key, out var journey ))
            {
                journey = new Journey();
                journey.Id = code;
                destinations.TryGetValue( csv.GetString( 10 ), out journey.dest );
                lines.TryGetValue( csv.GetString( 4 ), out journey.line );
                journey.stops = new List<Stop>();
                journeys.Add( key, journey );
            }
            stops.TryGetValue( csv.GetString( 8 ), out var stop );
            journey.stops.Add( stop );
        }

        void LoadTMIFile( string tag, ZipArchiveEntry entry, Action<CsvDataReader> cb )
        {
            if (entry.Name.StartsWith( tag ) && entry.Name.EndsWith( "TMI" ) && !entry.Name.EndsWith( "DTMI" )/*Dead*/)
            {
                curEntry = entry.Name;
                using var sr = new StreamReader(entry.Open());
                using var csv = CsvDataReader.Create(sr);
                while (csv.Read()) { cb( csv ); }
            }
        }

        public void LoadAll( string folder )
        {
            string strExeFilePath = Assembly.GetExecutingAssembly().Location;
            string strWorkPath = Path.GetDirectoryName(strExeFilePath);
            string searchPath = Path.Combine(strWorkPath, folder);

            if (!Directory.Exists( searchPath ))
            {
                Console.WriteLine( searchPath + " does not exist, nothing loaded." );
                return;
            }

            // Load TMI files
            var zippedTmiArchives = Directory.GetFiles(searchPath, "*.zip", SearchOption.AllDirectories);

            foreach (var zipPath in zippedTmiArchives)
            {
                // Store curZipPath for error handling in case we find duplicates, it can be logged and linked to the current extracting file.
                curZipPath = Path.GetFileName( zipPath );
                if (zipPath == null)
                    continue;

                using var archive = ZipFile.OpenRead(zipPath);
                foreach (ZipArchiveEntry entry in archive.Entries)
                {
                    LoadTMIFile( "LINE", entry, LoadLine );
                    LoadTMIFile( "DEST", entry, LoadDest );
                    LoadTMIFile( "POINT", entry, LoadStop );
                    //	LoadTMIFile( "LINK", entry, LoadLink );
                }
                var jopali = archive.Entries.Where(e => e.Name.Contains("JOPATILI"));
                foreach (ZipArchiveEntry entry in jopali)
                {
                    LoadTMIFile( "JOPATILI", entry, LoadJourney );
                }

                Console.WriteLine( "TMI Loaded " + Path.GetFileName( zipPath ) );
            }
        }
    }

}