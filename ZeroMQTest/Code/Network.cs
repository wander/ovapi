﻿using System.Diagnostics;
using System.Numerics;

namespace OVAPI.Code
{
    public interface ILine
    {
        string Name { get; } // Eg. Eindhoven Central Station - Eindhoven Airport.
        string LinePlanningNumber { get; } // Private number.
        string LineDisplayNumber { get; } // May contain character, e.g. C11.
        string TransportType { get; } // tram, bus, train, ferry.
    }

    public interface IDestination
    {
        string Name { get; }
        string Via { get; }
    }

    public interface IStop
    {
        public string Name { get; }
        public Vector2 Point { get; }

    }

    public interface IRoute
    {
        // Note1: dir is normalized.
        // Note2: the dist in last point is 0, because there is no next point and the dir is a zero vector.
        public (Vector2 point, float dist, Vector2 dir) Point( int idx );
        public int NumPoints { get; }
    }

    public interface IJourney
    {
        public ILine Line { get; }
        public IRoute Route { get; }
        public IDestination Destination { get; }
        public (IStop stop, IDestination dest) Stop( int i );
        public int NumStops { get; }

        public string AsMsg()
        {
            string msg = $"{Line.Name}|{Line.LinePlanningNumber}|{Line.TransportType}|{Line.LineDisplayNumber}|{NumStops}|";
            string prevDest = string.Empty;
            for (int i = 0;i < NumStops;i++)
            {
                var st = Stop(i);
                string dest = string.Empty;
                if (st.dest != null && st.dest.Name != prevDest)
                {
                    prevDest = st.dest.Name;
                    dest = prevDest;
                }
                msg += $"{st.stop.Name}|{st.stop.Point.X}|{st.stop.Point.Y}|{dest}|";
            }
            msg += $"{Route.NumPoints}|";
            for (int i = 0;i < Route.NumPoints;i++)
            {
                var pnt = Route.Point(i);
                msg += $"{pnt.point.X}|{pnt.point.Y}|";
            }
            return msg;
        }
    }

    public interface INetwork
    {
        // Owner is: QBUZZ, EBS, RET, CXX, ARR, KEOLIS, etc. 
        public IJourney FindJourney( string owner, string journeyNumber, string priveLineNumber );

        public IStop FindStop( string owner, string userStopCode );
    }

    public static class NetworkUtils
    {
        // Given a vehicle's position and journey interpolate the postion along the route in the journey.
        // Returns false, if at end of journey.
        // The returned interpolant.point should be the input for the next request. While the pntCatMull,
        // contains a more smoothish output of the request. The angle of the interpolant is also based on the catMull tangent.
        static public bool FindPositionInJourney(
            IJourney journey,
            ref int prevPointIndex,
            Vector2 oldPosition,
            float advanceDistance,
            out (Vector2 pnt, float dst, Vector2 dir) interpolant,
            out Vector2 pntCatMull
            )
        {
            Debug.Assert( advanceDistance >= 0, "Invalid advance distance." );

            // If already at end, just return last point.
            var route = journey.Route;
            if (prevPointIndex >= route.NumPoints-1)
            {
                interpolant = route.Point( route.NumPoints-1 );
                pntCatMull  = interpolant.pnt;
                return false;
            }

            // Find closest point on route.
            if (prevPointIndex < 0)
            {
                float closestDist = 1e30f;

                for (int i = 0;i< route.NumPoints;i++)
                {
                    var p00 = route.Point(i);
                    var distSq = Vector2.DistanceSquared(oldPosition, p00.point);
                    if (distSq < closestDist)
                    {
                        closestDist  = distSq;
                        prevPointIndex = i;
                    }
                }
                Debug.Assert( prevPointIndex >= 0 );
            }

            // Find point in between two points (interpolant/startPoint).
            Vector2 startPoint;
            int nextPointIdx;
            if (prevPointIndex == 0)
            {
                nextPointIdx = 1;
                GetPointInSegment( oldPosition, route.Point( 0 ).point, route.Point( 1 ).point, out startPoint );
            }
            else if (prevPointIndex == route.NumPoints-1)
            {
                nextPointIdx = route.NumPoints-1;
                GetPointInSegment( oldPosition, route.Point( route.NumPoints-2 ).point, route.Point( route.NumPoints-1 ).point, out startPoint );
            }
            else
            {
                nextPointIdx = prevPointIndex+1;
                while (GetPointInSegment( oldPosition, route.Point( prevPointIndex ).point, route.Point( nextPointIdx ).point, out _ ) == 1)
                {
                    prevPointIndex++;
                    nextPointIdx++;
                    if (prevPointIndex >= route.NumPoints-1)
                    {
                        interpolant = route.Point( route.NumPoints-1 );
                        pntCatMull  = interpolant.pnt;
                        return false;
                    }
                }
                while (GetPointInSegment( oldPosition, route.Point( prevPointIndex ).point, route.Point( nextPointIdx ).point, out startPoint ) == -1)
                {
                    prevPointIndex--;
                    nextPointIdx--;
                    if (prevPointIndex == 0)
                    {
                        break;
                    }
                }
            }

            var prevPoint = startPoint;
            float distToNextPoint = (route.Point(nextPointIdx).point-startPoint).Length();
            float movedDistance = 0;

            while (advanceDistance > distToNextPoint)
            {
                if (nextPointIdx == route.NumPoints-1)
                {
                    // We also know that (advanceDistance is > distToNextPoint), so we set interpolant to last point.
                    prevPointIndex = nextPointIdx;
                    interpolant = route.Point( nextPointIdx ); // Last point.dir is copied from second last point.
                    pntCatMull  = interpolant.pnt;
                    interpolant.dst = movedDistance+distToNextPoint;
                    return false;
                }
                advanceDistance -= distToNextPoint;
                movedDistance  += distToNextPoint;
                prevPoint       = route.Point( nextPointIdx ).point;
                distToNextPoint = route.Point( nextPointIdx ).dist;
                nextPointIdx++;
            }
            Debug.Assert( advanceDistance <= distToNextPoint );
            prevPointIndex = nextPointIdx-1;
            var endPoint = prevPoint + route.Point(prevPointIndex).dir*advanceDistance;
            float t = (endPoint-route.Point(prevPointIndex).point).Length() / route.Point(prevPointIndex).dist;
            Vector2 p0, p1, p2, p3;
            if (nextPointIdx-2<0)
            {
                p0 = route.Point( 0 ).point - route.Point( 0 ).dir;
            }
            else
            {
                p0 = route.Point( nextPointIdx-2 ).point;
            }
            if (nextPointIdx+1==route.NumPoints)
            {
                p3 = route.Point( route.NumPoints -1 ).point + route.Point( route.NumPoints -1 ).dir;
            }
            else
            {
                p3 = route.Point( nextPointIdx+1 ).point;
            }
            p1 = route.Point( prevPointIndex ).point;
            p2 = route.Point( nextPointIdx ).point;
            var pos1 = CatmullRom( t, p0, p1, p2, p3, 0.5f );
            var pos2 = CatmullRom( t+0.1f, p0, p1, p2, p3, 0.5f );
            var tangent = Vector2.Normalize(pos2-pos1);
            pntCatMull   = pos1;
            interpolant  = (endPoint, movedDistance+advanceDistance, tangent);
            if (!(MathF.Abs( interpolant.dir.Length()-1 ) < 0.01f))
            {
                interpolant.dir = new Vector2( 1, 0 );
            }
            return true;
        }

        public static Vector2 CatmullRom( float t, Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3, float alpha = 0.5f )
        {
            float t0 = 0.0f;
            float t1 = GetT(t0, alpha, p0, p1);
            float t2 = GetT(t1, alpha, p1, p2);
            float t3 = GetT(t2, alpha, p2, p3);

            t = Vector2.Lerp( new Vector2( t1, 0 ), new Vector2( t2, 0 ), t ).X;

            var A1 = (t1 - t) / (t1 - t0) * p0 + (t - t0) / (t1 - t0) * p1;
            var A2 = (t2 - t) / (t2 - t1) * p1 + (t - t1) / (t2 - t1) * p2;
            var A3 = (t3 - t) / (t3 - t2) * p2 + (t - t2) / (t3 - t2) * p3;

            var B1 = (t2 - t) / (t2 - t0) * A1 + (t - t0) / (t2 - t0) * A2;
            var B2 = (t3 - t) / (t3 - t1) * A2 + (t - t1) / (t3 - t1) * A3;

            var C = (t2 - t) / (t2 - t1) * B1 + (t - t1) / (t2 - t1) * B2;

            return C;
        }

        public static float GetT( float t, float alpha, Vector2 p0, Vector2 p1 )
        {
            var d = p1 - p0;
            float a = Vector2.Dot(d, d);
            float b = MathF.Pow(a, alpha * 0.5f);
            return (b + t);
        }

        static int GetPointInSegment( Vector2 p, Vector2 v0, Vector2 v1, out Vector2 interpolated )
        {
            var line = v1 - v0;
            var pointToStart = p - v0;
            var length = line.Length();
            var dir = line / length;
            var projection = Vector2.Dot(pointToStart, dir);

            // Closest point is before.
            if (projection < 0)
            {
                interpolated = v0;
                return -1;
            }

            // Closest point is beyond.
            if (projection > length)
            {
                interpolated = v1;
                return 1;
            }

            // Closest point is along the line segment.
            interpolated = v0 + dir * projection;
            return 0;
        }

        public static bool Sane( float f )
        {
            return !(float.IsNaN( f ) || float.IsInfinity( f ));
        }

        public static bool Sane( Vector2 v )
        {
            return Sane( v.X ) && Sane( v.Y );
        }
    }
}
