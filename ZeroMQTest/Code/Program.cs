﻿using OVApI;
using OVAPI.Code;
using OVAPI.Code.OVParsers;
using System.Diagnostics;
using WanderOV;

//UnitTests unitTests = new UnitTests();
////unitTests.TestExtrapolation();
//unitTests.TestCatMullRom();


while (true)
{
    try
    {
        App p = new();
        p.Run();
    }
    catch (Exception e)
    {
        Console.WriteLine( e.Message );
        Console.WriteLine( e.StackTrace );
        Console.WriteLine( "Restarting.." );
    }

}

class App
{
    ActiveOV activeOV;
    Broker broker;
    OVRequester requester;
    INetwork network;

    public App()
    {
        Netex netex = new Netex();
        netex.LoadAll( "_database" );
        netex.PrintStats();

        network = netex;

        activeOV  = new ActiveOV(network);
        broker    = new Broker( 9999 );
        requester = new OVRequester();
    }

    public void Run()
    {
        Stopwatch sw = new Stopwatch();
        while (true)
        {
            sw.Restart();

            // See if new data is available and process accordingly.
            if (requester.PeekLiveOVData(out var updatedVehicles ))
            {
                activeOV.UpdateVehicles( updatedVehicles );

                // Accept and remove clients and process requests from clients.
                broker.Update();

                // Requests to the broker results in messages, process them.
                for (BrokerMsg msg = broker.GetMessage();msg !=null;msg = broker.GetMessage())
                {
                    switch (msg.fields[0])
                    {
                        case "vehicles": // Advance sim.
                            if (msg.fields.Length>=5)
                            {
                                msg.client.UpdateInterestArea( float.Parse( msg.fields[1] ), float.Parse( msg.fields[2] ), float.Parse( msg.fields[3] ), float.Parse( msg.fields[4] ) );
                            }
                            activeOV.AdvanceSimulationToNow();
                            var vehicles = activeOV.GetStateAsCSV();
                            if (vehicles != null)
                            {
                                broker.SendMessage( "vehicles", vehicles, msg.from );
                            }
                            break;

                        case "journey": // Expects owner, journeyNumber.
                            var journey = network.FindJourney( msg.fields[1], msg.fields[2], msg.fields[4] );
                            if (journey!=null)
                            {
                                // field[1] is owner, field[2] is journeyNumber, field[3] is vehicleNumber
                                broker.SendMessage( "journey", string.Join( '|', msg.fields[1], msg.fields[3], journey.AsMsg() ), msg.from );
                            }
                            break;
                    }
                }
            }



            long timeToWait = 50-sw.ElapsedMilliseconds;
            if (timeToWait > 0) Thread.Sleep( (int)timeToWait );
        }
    }
}



