﻿using NetMQ;
using NetMQ.Sockets;
using System.IO.Compression;
using System.Text;
using System.Xml;

namespace WanderOV
{

    /*
        * 
        * 
       BISON (KV6, KV15, KV17)
       =======================

       URI: tcp://pubsub.besteffort.ndovloket.nl:7658/

       Bekende envelopes:
       /ARR/KV15messages    (Arriva)
       /ARR/KV17cvlinfo
       /ARR/KV6posinfo
       /CXX/KV15messages    (Connexxion, Breng, OV Regio IJsselmond)
       /CXX/KV17cvlinfo
       /CXX/KV6posinfo
       /DITP/KV15messages   (U-OV Sneltram)
       /DITP/KV17cvlinfo
       /DITP/KV6posinfo
       /EBS/KV15messages    (EBS)
       /EBS/KV17cvlinfo
       /EBS/KV6posinfo
       /GVB/KV15messages    (GVB)
       /GVB/KV17cvlinfo
       /GVB/KV6posinfo
       /OPENOV/KV6posinfo   (De Lijn)
       /OPENOV/KV15messages (OpenEBS)
       /OPENOV/KV17cvlinfo
       /QBUZZ/KV15messages  (QBuzz)
       /QBUZZ/KV17cvlinfo
       /QBUZZ/KV6posinfo
       /RIG/KV15messages    (HTM, RET, Veolia)
       /RIG/KV17cvlinfo
       /RIG/KV6posinfo
       /SYNTUS/KV6posinfo   (Syntus)
       /SYNTUS/KV15message
       /SYNTUS/KV17cvlinfo


       InfoPlus DVS
       ============

       URI: tcp://pubsub.besteffort.ndovloket.nl:7664/

       Bekende envelopes:
       /RIG/InfoPlusDVSInterface4 (DVS PPV)
       /RIG/InfoPlusPILInterface5
       /RIG/InfoPlusPISInterface5
       /RIG/InfoPlusVTBLInterface5
       /RIG/InfoPlusVTBSInterface5
       /RIG/NStreinpositiesInterface5
       /RIG/InfoPlusRITInterface2

    */

    public enum VehicleInfoType
    {
        Owner,
        LinePrivateNumber,
        JourneyNumber,
        VehicleNumber,
        Punctuality,
        Rdx,
        Rdy,
        StopCode,
        Timestamp,
        Unknown
    }

    public struct VehicleInfo
    {
        public string[]infos;

        public string LinePlanningNumb => infos[(int)VehicleInfoType.LinePrivateNumber];
        public string JournyNumb => infos[(int)VehicleInfoType.JourneyNumber];
        public string Owner => infos[(int)VehicleInfoType.Owner];
        public string Number => infos[(int)VehicleInfoType.VehicleNumber];
        public string ID => string.Join( ':', Owner, Number );
    }


    public class OVRequester
    {
         SubscriberSocket socket;

        public OVRequester()
        {
            socket = new SubscriberSocket();
            socket.Connect( "tcp://pubsub.besteffort.ndovloket.nl:7658" );
            RegisterEnvelopes().ForEach( e=> socket.Subscribe(e));
        }

        List<string> RegisterEnvelopes()
        {
            var envelopes = new List<string>
            {
                "/CXX/KV6posinfo",
                "/ARR/KV6posinfo",
                "/DITP/KV6posinfo",
                "/EBS/KV6posinfo",
                "/OPENOV/KV6posinfo",
                "/GVB/KV6posinfo",
                "/QBUZZ/KV6posinfo",
                "/RIG/KV6posinfo",
                "/SYNTUS/KV6posinfo"
            };
            return envelopes;
        }

        // Blocking call.
        public bool PeekLiveOVData(out Dictionary<string, VehicleInfo> vehicles)
        {
            vehicles = new();
            try
            {
                while (socket.HasIn)
                {
                    var topic = socket.ReceiveFrameString();
                    var buff  = socket.ReceiveFrameBytes();
                    using var gs = new GZipStream( new MemoryStream(buff), CompressionMode.Decompress );

                    // Little awkward here, first convert binary to string (UTF-8) and then back. has to do with BOM being either avaialble or not, just directly reading bytes in 
                    // XML reader fails (TODO: fix this).
                    using var sr = new StreamReader( gs );
                    var xml = sr.ReadToEnd();
                    using var stream = new MemoryStream( Encoding.UTF8.GetBytes( xml ) );

                    UpdateVehicleInfo( vehicles, stream );
                }
            }
            catch (Exception e)
            {
                Console.WriteLine( e.ToString() );
            }
            return vehicles.Count!=0;
        }

        bool IsUsefulXmlNode(XmlReader reader)
        {
            return reader.Name.EndsWith( "ONROUTE" ) || reader.Name.EndsWith( "OFFROUTE" ) || reader.Name.EndsWith( "ARRIVAL" ) ||
                   reader.Name.EndsWith( "ONSTOP" )  || reader.Name.EndsWith( "DEPARTURE" );
        }

        void UpdateVehicleInfo( Dictionary<string, VehicleInfo> vehicles, Stream stream )
        {
            var vehicle  = new VehicleInfo();
            var type = VehicleInfoType.Unknown;
            bool onRoute = false;

            using var reader = XmlReader.Create(stream);
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            type = VehicleInfoType.Unknown;
                            if (IsUsefulXmlNode(reader))
                            {
                                onRoute = true;
                                vehicle.infos = new string[9];
                            }
                            if (reader.Name.EndsWith( "dataownercode" )) type = VehicleInfoType.Owner;
                            if (reader.Name.EndsWith( "lineplanningnumber" )) type = VehicleInfoType.LinePrivateNumber;
                            if (reader.Name.EndsWith( "journeynumber" )) type = VehicleInfoType.JourneyNumber;
                            if (reader.Name.EndsWith( "vehiclenumber" )) type = VehicleInfoType.VehicleNumber;
                            if (reader.Name.EndsWith( "punctuality" )) type = VehicleInfoType.Punctuality;
                            if (reader.Name.EndsWith( "rd-x" )) type = VehicleInfoType.Rdx;
                            if (reader.Name.EndsWith( "rd-y" )) type = VehicleInfoType.Rdy;
                            if (reader.Name.EndsWith( "userstopcode" )) type = VehicleInfoType.StopCode;
                            if (reader.Name.EndsWith( "timestamp" )) type = VehicleInfoType.Timestamp;
                            break;

                        case XmlNodeType.Text:
                            if (onRoute && type != VehicleInfoType.Unknown )
                            {
                                vehicle.infos[(int)type] = reader.Value;
                                type = VehicleInfoType.Unknown;
                            }
                            break;


                        case XmlNodeType.EndElement:
                            if (IsUsefulXmlNode(reader))
                            {
                                if (onRoute)
                                {
                                    var id = vehicle.ID;
                                    if (vehicles.TryGetValue( id, out var existingVehicle ))
                                    {
                                        var t1 = DateTime.Parse( existingVehicle.infos[(int)VehicleInfoType.Timestamp] );
                                        var t2 = DateTime.Parse( vehicle.infos[(int)VehicleInfoType.Timestamp] );
                                        if (t2 > t1)
                                        {
                                            vehicles.Remove( id );
                                            vehicles.Add( id, vehicle );
                                        }
                                    }
                                    else
                                    {
                                        vehicles.Add( id, vehicle );
                                    }
                                }
                                onRoute = false;
                            }
                            break;
                    }
                }
            }
        }
    }

}