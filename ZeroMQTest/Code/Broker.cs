﻿
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

namespace WanderOV
{
    public class Client
    {
        internal TcpClient tcp;
        internal string data;
        internal float xmin, xmax, ymin, ymax; // Interest area.

        public void UpdateInterestArea(float _xmin, float _xmax, float _ymin, float _ymax)
        {
            xmin = _xmin;
            xmax = _xmax;
            ymin = _ymin;
            ymax = _ymax;
        }
    }

    public class BrokerMsg
    {
        public Client client;
        public TcpClient from;
        public string[] fields;
    }


    public class Broker
    {
        TcpListener listener;
        List<Client> clients;
        Queue<BrokerMsg> messages;
        Stopwatch stopwatch;
        long lastAliveCheck;

        public Broker( int port )
        {
            messages = new Queue<BrokerMsg>();
            clients  = new List<Client>();
            listener = new TcpListener( System.Net.IPAddress.Any, port );
            stopwatch = new Stopwatch();
            stopwatch.Start();
            listener.Start();
        }

        public void Update()
        {
            CleanupDisconnectedClients();
            AcceptClients();
            ProcessRequests();
        }

        public BrokerMsg GetMessage()
        {
            if (messages.Count == 0)
                return null;
            return messages.Dequeue();
        }

        void ProcessRequests()
        {
            for (int i = 0;i < clients.Count;i++)
            {
                int bytesAvailable = clients[i].tcp.Available;
                if (bytesAvailable > 0)
                {
                    byte [] newBytes = new byte[bytesAvailable];
                    int bytesRead = clients[i].tcp.GetStream().Read( newBytes, 0, bytesAvailable );
                    var newString = Encoding.UTF8.GetString( newBytes, 0, bytesRead );
                    clients[i].data += newString;
                    var data = clients[i].data;
                    while (true)
                    {
                        int splitIdx = data.IndexOf( "####" );
                        if (splitIdx >= 0)
                        {
                            var msg = data.Substring( 0, splitIdx );
                            data = data.Substring( splitIdx+4, data.Length-(splitIdx+4) );
                            ProcessMessage( msg, clients[i].tcp );
                        }
                        else
                        {
                            clients[i].data = data;
                            break;
                        }

                    }
                }
            }
        }

        void ProcessMessage( string msg, TcpClient from )
        {
            // Messages are a single line ending with ####. The single line may have multiple fields and are seperated by an | .
            var fields = msg.Split('|');
            if (fields.Length == 0)
            {
                Console.WriteLine( $"Invalid msg {msg}" );
                return;
            }
            BrokerMsg brokerMsg = new BrokerMsg();
            brokerMsg.fields = fields;
            brokerMsg.from = from;
            messages.Enqueue( brokerMsg );
        }

        public void SendMessage( string id, string msg, TcpClient to )
        {
            string payload = $"{id}|{msg}####";
            to.GetStream().WriteAsync( Encoding.UTF8.GetBytes( payload ) );
        }

        void AcceptClients()
        {
            while (listener.Pending())
            {
                var newClient = listener.AcceptTcpClient();
                newClient.NoDelay = true;
                var endpoint = newClient.Client.RemoteEndPoint;
                if (endpoint != null)
                {
                    Console.WriteLine( DateTime.Now + " " + endpoint.ToString() + " Connected." );
                }
                clients.Add( new Client() { tcp=newClient, data=string.Empty } );
            }
        }

        void CleanupDisconnectedClients()
        {
            if (clients==null)
                return;

            if (stopwatch.ElapsedMilliseconds - lastAliveCheck < 2000)
                return;
            lastAliveCheck=stopwatch.ElapsedMilliseconds;

            IPGlobalProperties ipProperties = IPGlobalProperties.GetIPGlobalProperties();


            for (int i = 0;i < clients.Count;i++)
            {
                // .Connected property on client does not contain valid info.
                TcpConnectionInformation? info = ipProperties.GetActiveTcpConnections().Where(x =>
                    x.LocalEndPoint.Equals(clients[i].tcp.Client.LocalEndPoint) &&
                    x.RemoteEndPoint.Equals(clients[i].tcp.Client.RemoteEndPoint)).FirstOrDefault();

                if (info == null || info.State != TcpState.Established)
                {
                    var endpoint = clients[i].tcp.Client.RemoteEndPoint;
                    if (endpoint!=null)
                    {
                        Console.WriteLine( DateTime.Now + " " + endpoint.ToString() + " Left." );
                    }
                    clients.RemoveAt( i );
                    i--;
                }
            }
        }
    }

}