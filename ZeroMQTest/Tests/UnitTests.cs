﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OVAPI.Code;
using OVAPI.Code.OVParsers;
using System.Diagnostics;
using System.Numerics;

namespace OVApI
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void TestExtrapolation()
        {
            Netex netex = new Netex();
            netex.LoadAll( "_database" );
            netex.PrintStats();

            var journey = netex.FindJourney( "CXX", "414", "E303" );

            int prevPos = -1;
            var prevPoint = journey.Route.Point( 0 ).point;
            float routeTotalDist = 0;
            for (int i = 0; i < journey.Route.NumPoints; i++)
            {
                routeTotalDist += journey.Route.Point( i ).dist;
            }

            float advDist = 1;
            int kIters = 0;
            float totDist = 0;

            (Vector2 pnt, float dst, Vector2 dir) interpolant;
            Vector2 catmullPnt;
            while ( NetworkUtils.FindPositionInJourney( journey, ref prevPos, prevPoint, advDist, out interpolant, out catmullPnt) )
            {
                var len = interpolant.dir.Length();
                Debug.Assert( NetworkUtils.Sane( len ) );
                Debug.Assert( NetworkUtils.Sane( interpolant.dir ) );
                kIters++;
                totDist += interpolant.dst;
                prevPoint = interpolant.pnt;
            }
            totDist += interpolant.dst;

            var delta =  MathF.Abs( totDist-routeTotalDist );
            Assert.IsTrue( delta < 0.01f ); // This only works, if the first point on the route is not halfway somewhere.
        }

        [TestMethod]
        public void TestCatMullRom()
        {
            Vector2 p0, p1, p2, p3;
            p0 = new Vector2( 0, 0 );
            p1 = new Vector2( 1, 0 );
            p2 = new Vector2( 1, 1 );
            p3 = new Vector2( 1, 2 );

            int kIters = 1000;
            float t = 0.1f;

            while (kIters-- > 0)
            {
                float t2 = t+0.1f;
                var C1 = NetworkUtils.CatmullRom( t, p0, p1, p2, p3, 1 );
                var C2 = NetworkUtils.CatmullRom( t2, p0, p1, p2, p3, 1 );
                var tangent = Vector2.Normalize( (C2 - C1 ));
                Assert.IsTrue( NetworkUtils.Sane( tangent ) );
                t += 0.1f;
            }
            int kt = 0;
        }
    }
}