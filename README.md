## OVAPI Broker

Queries live OV-info from NDOVLoket and bundles the data to provide methods for usage in applications.

## Dependencies
- None

### Usage:

1. Start the .exe after build.
2. Optionally put it in a web server (ISS 8) using docker.


![](./sample0.gif)
